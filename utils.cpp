#include <algorithm>
#include <tuple>
#include "big_integer.h"
#include "utils.h"

void add_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two) {
    const auto TWO_SIZE = two.size();
    one.resize(std::max(one.size(), TWO_SIZE), 0);
    big_integer::chunk_t carry = 0;
    for (std::size_t i = 0; i < TWO_SIZE; ++i) { // might remove size() call
        carry = add_with_carry(one[i], two[i], carry);
    }
    for (std::size_t i = TWO_SIZE; i < one.size() and carry != 0; ++i) {
        carry = (big_integer::chunk_t) (++one[i] == 0);
    }
    if (carry != 0) {
        one.push_back(carry);
    }
}

void subtract_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two) {
    subtract_magnitude(one, one, two);
}

void subtract_magnitude(big_integer::magnitude_t &result,
                        const big_integer::magnitude_t &one,
                        const big_integer::magnitude_t &two) {
    shifted_subtract_magnitude(result, one, two, 0);
}

void multiply_by_chunk(big_integer::magnitude_t &one, const big_integer::chunk_t &two) {
    if (two == 0) {
        one.clear();
        return;
    }
    big_integer::chunk_t carry = 0;
    for (auto &chunk : one) {
        auto mul_carry = multiply_with_carry(chunk, two);
        carry = add_with_carry(chunk, carry);
        carry += mul_carry;
    }
    if (carry != 0) {
        one.push_back(carry);
    }
}

big_integer::chunk_t divide_by_chunk(big_integer::magnitude_t &one, const big_integer::chunk_t &two) {
    big_integer::chunk_t carry = 0;
    for (auto it = one.rbegin(); it != one.rend(); ++it) {
        divide_with_carry(carry, *it, two);
    }
    return carry;
}

int compare_magnitude(const big_integer::magnitude_t &one, const big_integer::magnitude_t &two) {
    return shifted_compare_magnitude(one, two, 0);
}

void increment(big_integer::magnitude_t &magnitude) {
    big_integer::chunk_t last_result = 0;
    for (std::size_t i = 0; i < magnitude.size() and last_result == 0; ++i) {
        last_result = ++magnitude[i];
    }
    if (last_result == 0) {
        magnitude.push_back(1);
    }
}

void decrement(big_integer::magnitude_t &magnitude) {
    constexpr auto UPPER_BOUND = (big_integer::chunk_t) -1;
    auto last_result = UPPER_BOUND;
    for (std::size_t i = 0; i < magnitude.size() and last_result == UPPER_BOUND; ++i) {
        last_result = --magnitude[i];
    }
    if (last_result == UPPER_BOUND) {
        magnitude.clear();
    }
}

void strip_zeroes(big_integer::magnitude_t &magnitude) {
    auto last_nonzero = magnitude.size() - 1;
    while (last_nonzero != -1 and magnitude[last_nonzero] == 0) {
        --last_nonzero;
    }
    magnitude.resize(last_nonzero + 1);
}

big_integer::magnitude_t divide_magnitude(big_integer::magnitude_t &dividend, big_integer::magnitude_t divisor) {
    using chunk_t = big_integer::chunk_t;
    using magnitude_t = big_integer::magnitude_t;
    dividend.push_back(0);              // cause 82835 / 367 = 225 and 12835 / 367 = 34
    magnitude_t quotient = magnitude_t(dividend.size() - divisor.size(), 0);
    normalize(dividend, divisor);
    for (std::size_t i = quotient.size(); i-- > 0;) {
        chunk_t trial = estimate(dividend, divisor, i);
        magnitude_t estimation = divisor;
        multiply_by_chunk(estimation, trial);
        if (shifted_compare_magnitude(dividend, estimation, i) < 0) {
            --trial;
            subtract_magnitude(estimation, divisor);
        }
        quotient[i] = trial;
        shifted_subtract_magnitude(dividend, estimation, i);
        if (dividend.empty()) {
            break;
        }
    }
    std::swap(quotient, dividend);
    return dividend;
}

void normalize(big_integer::magnitude_t &n, big_integer::magnitude_t &d) {
    big_integer::chunk_t f = std::numeric_limits<big_integer::chunk_t>::max() / (d.back() + 1);
    multiply_by_chunk(n, f);
    multiply_by_chunk(d, f);
}

big_integer::chunk_t estimate(const big_integer::magnitude_t &n, const big_integer::magnitude_t &d, std::size_t shift) {
    static constexpr big_integer::chunk_t zero = 0;
    if (compare_magnitude(n, d) < 0) {
        return 0;
    }
    size_t end = d.size() + shift;
    big_integer::magnitude_t n3{n[end - 2], n[end - 1], end == n.size() ? zero : n[end]};
    big_integer::magnitude_t d2(d.end() - 2, d.end());
    big_integer::chunk_t estimation;
    if (d2[1] > n3[2]) {
        big_integer::chunk_t dummy = 0;
        divide_with_carry(dummy = n3[2], estimation = n3[1], d2[1]);
    } else {
        estimation = std::numeric_limits<big_integer::chunk_t>::max();
    }
    multiply_by_chunk(d2, estimation);
    if (compare_magnitude(n3, d2) < 0) {
        --estimation;
    }
    return estimation;
}

int shifted_compare_magnitude(const big_integer::magnitude_t &one,
                              const big_integer::magnitude_t &two,
                              std::size_t shift) {
    if (one.size() < two.size() + shift) {
        return -1;
    }
    if (one.size() > two.size() + shift) {
        return 1;
    }
    std::size_t pointer = one.size();
    while (pointer-- > shift) {
        big_integer::chunk_t op1 = one[pointer];
        big_integer::chunk_t op2 = pointer < shift ? big_integer::chunk_t(0) : two[pointer - shift];
        if (op1 != op2) {
            return compare(op1, op2);
        }
    }
    return 0;
}

void shifted_subtract_magnitude(big_integer::magnitude_t &one, const big_integer::magnitude_t &two, std::size_t shift) {
    shifted_subtract_magnitude(one, one, two, shift);
}

void shifted_subtract_magnitude(big_integer::magnitude_t &result,
                                const big_integer::magnitude_t &one,
                                const big_integer::magnitude_t &two,
                                std::size_t shift) {
    using chunk_t = big_integer::chunk_t;
    result.resize(one.size());
    chunk_t borrow = 0;
    const auto TWO_SIZE = two.size() + shift;
    for (std::size_t i = shift; i < TWO_SIZE; ++i) {
        auto subtrahend = two[i - shift];
        borrow = subtract_with_carry(result[i] = one[i], subtrahend, borrow);
    }
    for (auto i = TWO_SIZE; borrow != 0; ++i) {
        borrow = (chunk_t) (--(result[i] = one[i]) == std::numeric_limits<chunk_t>::max());
    }
    strip_zeroes(result);
}
